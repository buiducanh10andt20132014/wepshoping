import React, { Component } from 'react';
import './App.css';
import ProductsContainer from './containers/productContainer';
import iphoneContainer from './containers/iphoneContainer';
import SamsungContainer from './containers/samsungContainer';
import oppoContainer from './containers/oppoContainer';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import CartContainer from './containers/cartContainer';
import ProfileContainer from './containers/profileContainer';
import Search from './components/search';
import CallApi from './Api/CallApi';

class App extends Component {
  onToProfile = () => {
  }
  render() {
    return (
      <>
      <CallApi />
      <Router>
        
        <div>
          <div className="banner-media">
            <div className="item">
              <img src="https://cdn.tgdd.vn/2021/06/banner/1200-44-1200x44-1.png" alt="Mê ơ-rô" />
            </div>
          </div>
          {/* <Header /> */}
          <main id="mainContainer">
            <div className="App">
              <nav className="head">
                <ul className="nav navbar-var" >
                  <li className="nav-item is-active" active-color="DarkSlateBlue" >
                    <Link to="/" className="my-link"  > Trang Chủ </Link>
                  </li>
                  <li className="nav-item" active-color="DarkKhaki" >
                    <Link to="/cart" className="my-link"  > Giỏ Hàng </Link>
                  </li>
                  <li className="nav-item" active-color="DarkKhaki" >
                    <Link to="/profile" className="my-link"  > Thông tin </Link>
                  </li>
                  <Search />
                  <li className="nav-item" active-color="DarkKhaki" >
                    <Link to="/iphone" className="my-link"  > IPhone </Link>
                  </li>
                  <li className="nav-item" active-color="DarkKhaki" >
                    <Link to="/samsung" className="my-link"  > Sam Sung </Link>
                  </li>
                  <li className="nav-item" active-color="DarkKhaki" >
                    <Link to="/Oppo" className="my-link"  > Oppo </Link>
                  </li>
                </ul>
                <Link to="/samsung" className="my-link" >
                  <marquee direction="right"
                    scrolldelay="5"
                    loop="20"
                    scrollamount="10"
                    behavior="alternate"
                    >
                    <img onClick={() => this.onToProfile()}
                      border="0" src="http://3.bp.blogspot.com/-pIummBi6N3M/UeOYg7UCyKI/AAAAAAAAACk/vl5vyv3IuO0/s1600/dienthoaididong_02.jpg">
                    </img>
                  </marquee>
                </Link>
              </nav>
            </div>

            {/* <Footer /> */}
            <Route path="/" exact component={ProductsContainer} />
            <Route path="/cart" component={CartContainer} />
            <Route path="/iphone" component={iphoneContainer} />
            <Route path="/samsung" component={SamsungContainer} />
            <Route path="/Oppo" component={oppoContainer} />
            <Route path="/profile/:id" component={ProfileContainer} />
          </main>
        </div>
      </Router>
      </>
    );
  }
}

export default App;


