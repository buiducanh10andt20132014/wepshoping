import axios from 'axios';
import { useEffect, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { actCallApi } from './../actions/index';
 
const CallApi = () => {
   const dispatch= useDispatch();
    const [data, setData] = useState()
    useEffect(() => {
        callData();
    }, []) 

    const callData = async () => {
        var data = await axios.get('https://60d06b9a7de0b200171089d8.mockapi.io/products')
        if(data) {
            setData(data.data);    
            console.log("data",data);
            dispatch(actCallApi(data))    
        } else {
            console.log('error')
        }
      }
  
    return(
        <div></div>
    )
}




export default CallApi;

