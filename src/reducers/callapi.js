import * as Types from './../constants/actionType';

var initialState= { callapi:[] };
const callapi = (state = initialState, action) => {
    switch (action.type) {
        case Types.CALL_API:
            console.log(action,"action")
            return { ...state, callapi : action.payload.data};
        default: return state;
    }
}


export default callapi;