import * as Types from './../constants/actionType';

var initialState= { search:"" };
const search = (state = initialState, action) => {
    switch (action.type) {
        case Types.TRANS_TO_VALUE:
            return { ...state, search : action.payload};
        default: return state;
    }
}


export default search;