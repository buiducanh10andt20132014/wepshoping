import { combineReducers } from 'redux';
import products from './products';
import cart from './cart';
import message from './message';
import search from './search';
import callapi from './callapi';
const appReducers = combineReducers({
    products,
    cart,
    message,
    search,
    callapi    
});

export default appReducers;

