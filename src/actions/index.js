import * as types from './../constants/actionType';


export const actAddToCart = (product, quantity) => {
    return {
        type:types.ADD_TO_CARD,
        product: product,
        quantity: quantity
    }
}

export const actCallApi = (data) =>{
     return {
        type : types.CALL_API,
        payload:data,
     }
}

export const actTransValue = (valueSearch) => {
    return {
        type : types.TRANS_TO_VALUE,
        payload:valueSearch,
    }
}

export const actChangeMessage = (message) => {
    return {
        type : types.CHANGE_MESSAGE,
        message : message
    }
}

export const actDeleteProductInCart = (product) => {
    return {
        type : types.DELETE_PRODUCT_IN_CART,
        product
    }
}
export const actUpdateProductInCart = (product, quantity) => {
    return {
        type : types.UPDATE_PRODUCT_IN_CART,
        product,
        quantity
    }
}