import React, { Component } from 'react';
import './../App.css';
import { connect } from 'react-redux';
import { actTransValue } from './../actions/index'

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valueSearch:'',
        }
    }
    handleValueSearch = (event) => {
        this.setState( {valueSearch: event.target.value }); 
    }
    handleSubmit = (event) =>{
        event.preventDefault();
        this.props.handleSubmit(this.state.valueSearch);
    }
    render() {
        var { valueSearch } = this.props;
        return (
            <form onSubmit={this.handleSubmit} >
                <div className="">
                    <input type="text" className="" placeholder="Search.." value={ this.state.valueSearch} onChange={this.handleValueSearch} />
                    <button type="submit" className="fa fa-search" />
                </div>
            </form>

        );
    }

}
const mapDispatchToProps = (dispatch, props) => {
    return {
        handleSubmit: (valueSearch) => {
            dispatch(actTransValue(valueSearch));
        }
    }
}

export default connect(null, mapDispatchToProps)(Search);

