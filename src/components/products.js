import React, { Component } from 'react';
import './../App.css';

class Products extends Component {
    render() {
        return (
            <section className="section">
                <div className ="banner-media" >
                    <h1 className="section-heading font">Danh Sách Sản Phẩm</h1>
                    <div className="row">
                        {this.props.children}
                    </div>
                </div>
            </section>
        );
    }
}



export default Products;
