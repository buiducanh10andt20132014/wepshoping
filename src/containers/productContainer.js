import React, { Component } from 'react';
import { connect } from 'react-redux';
import Products from './../components/products';
import Product from './../components/product';
import PropTypes from 'prop-types';
import { actAddToCart, actChangeMessage } from './../actions/index';


class ProductsContainer extends Component {
    render() {
        var { products } = this.props;
        return (
            <Products>
                {this.showProducts(products)}
            </Products>
        );
    }

    showProducts(products) {
        var result = null;
        var ItemFilter = [];
        var { onAddToCart, onChangeMessage } = this.props;
        if (this.props.search.search != null && (products != null) ) {
            ItemFilter = products.filter((item, index) => {
                return (item.name.toUpperCase().includes(this.props.search.search.toUpperCase()) == true)
            });
        } else { ItemFilter = products }
        if (ItemFilter.length > 0) {
            result = ItemFilter.map((product, index) => {
                return <Product
                    key={index}
                    product={product}
                    onAddToCart={onAddToCart}
                    onChangeMessage={onChangeMessage}
                />
            });
        }
        return result;
    }
}

ProductsContainer.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            image: PropTypes.string.isRequired,
            desciption: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            inventory: PropTypes.number.isRequired,
            rating: PropTypes.number.isRequired
        })
    ).isRequired,
    onChangeMessage: PropTypes.func.isRequired
}

const mapStateToProps = state => {
    console.log('statecallapi: ', state)
    return {
        products: state.callapi.callapi,
        search: state.search,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddToCart: (product) => {
            dispatch(actAddToCart(product, 1));
        },
        onChangeMessage: (message) => {
            dispatch(actChangeMessage(message));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);
