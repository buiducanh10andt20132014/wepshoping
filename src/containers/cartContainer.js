import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Cart from './../components/cart';
import * as message from './../constants/message';
import CartItem from './../components/cartItem1';
import CartResult from './../components/cartResult';
import { actDeleteProductInCart, actChangeMessage, actUpdateProductInCart } from './../actions/index'
// import messageContainer from './messageContainer';

class CartContainer extends Component {
    render() {
        var { cart } = this.props;
        return (
           <Cart>
               { this.showCartItem(cart) }
               { this.showTotalAmount(cart) }
           </Cart>
        );
    }
    showCartItem=(cart) =>{
        var { onDeleteProductInCart, onChangeMessage, onUpdateProductInCart } = this.props;
        var result = message.MSG_CART_EMPTY;
        if (cart.length > 0 ){
            result = cart.map((item, index) => {
                return (
                    <CartItem
                        key={index}
                        item={item}
                        index={index}
                        onDeleteProductInCart = { onDeleteProductInCart }
                        onChangeMessage = { onChangeMessage }
                        onUpdateProductInCart= { onUpdateProductInCart }  
                    />
                )
            })
        }
        return result;
    }
    showTotalAmount= (cart) => {
        var result = null;
        if (cart.length > 0){
            result = <CartResult cart={cart}/>
        }
        return result;
    }
    
}

CartContainer.propTypes = {
    cart : PropTypes.arrayOf(PropTypes.shape({
       product : PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            image: PropTypes.string.isRequired,
            desciption: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            inventory: PropTypes.number.isRequired,
            rating: PropTypes.number.isRequired
       }).isRequired,
       quantity : PropTypes.number.isRequired
    })).isRequired
}

const mapStateToProps = state =>{
    return {
        cart : state.cart
    }
}
const mapDispatchToProps = (dispatch, props)=>{
    return {
        onDeleteProductInCart : (product) => {
          dispatch(actDeleteProductInCart(product));
        },
        onChangeMessage : (message)=>{
            dispatch(actChangeMessage(message));
        },
        onUpdateProductInCart : (product, quantity) => {
            dispatch(actUpdateProductInCart(product, quantity));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer);
