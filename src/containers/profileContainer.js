import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actAddToCart, actChangeMessage } from './../actions/index';
import './../App.css';
class ProfileContainer extends Component {
  render() {
    var { products } = this.props;

    var product = this.showProducts(products);
     console.log("product", product)
    return (
      <div className="profile" >
        <div className="" >
          <div className="card  card-cascade">
            <div className="card-footer " >
              <div className="left img" >
                <div className="view overlay img ">
                  <img src={product[0].image}
                    className="img" alt={product[0].name} />
                  <a>
                    <div className="mask waves-light waves-effect waves-light"></div>
                  </a>
                  <ul className="rating">
                    <li>
                      Đánh Giá :{this.showRatings(product[0].rating)}
                    </li>
                  </ul>
                </div>
              </div>
              <div className="right rightImg">
                <div className="card-body ">
                  <h4 className="card-title  ">
                    <strong>
                      <a>Cấu Hình Điện Thoại {product[0].name} </a>
                    </strong>
                  </h4>
                  <p className="file">
                    {product[0].desciption}
                  </p>
                  <p className="">Giá :{product[0].price} vnd </p>
                  <div className="" >
                  <p className="file">Màn Hình :  {product[0].manhinh}</p>
                  <p className="" >Hệ Điều Hành :  {product[0].system}</p>
                  <p className="file">Cam Sau : {product[0].frontcam}</p>
                  <p className="">Cam Trước : {product[0].rearcam}</p>
                  <p className="file">Chip : {product[0].chip}</p>
                  <p className="">Pin : {product[0].pin}</p>
                  
                  </div>
                
                </div>
                
              </div>

            </div>
             
            <div className="card-footer">             
                <a
                  className="btn-floating blue-gradient"
                  data-toggle="tooltip"
                  data-placement="top"
                  title=""
                  data-original-title="Add to Cart"
                  onClick={() => this.onAddToCart(product[0])}
                >
                  <i className="fa fa-shopping-cart"></i>
                </a>
            </div>
          </div>
           <div>
          </div>
        </div>
      </div>
    );

  }
  showProducts(products) {
    var result = null;
    if (products.length > 0) {
      result = products.filter((product) => {
        return (this.props.match.params.id == product.id);
      });
    }
    return result;
  }
  onAddToCart = (product) => {
    this.props.onAddToCart(product);
  }
  showRatings(rating) {
    var result = [];
    for (var i = 1; i <= rating; i++) {
      result.push(<i key={i} className="fa fa-star"></i>);
    }
    for (var j = 1; j <= (5 - rating); j++) {
      result.push(<i key={i + j} className="fa fa-star-o"></i>);
    }
    return result;
  }
}
const mapStateToProps = state => {
  console.log('state: ', state)
  return {
    products: state.callapi.callapi
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onAddToCart: (product) => {
      dispatch(actAddToCart(product, 1));
    },
    onChangeMessage: (message) => {
      dispatch(actChangeMessage(message));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
