import React, { Component } from 'react';
import { connect } from 'react-redux';
import Products from './../components/products';
import Product from './../components/product';
import PropTypes from 'prop-types';
import { actAddToCart,actChangeMessage } from './../actions/index'


class SamsungContainer extends Component {
    render() {
        var { products } = this.props;
        return (
            <Products>
                { this.showProducts(products)}
            </Products>
        );
    }
    showProducts(products) {
        var result = null;
        var { onAddToCart,onChangeMessage } = this.props;
        if (products.length > 0) {
            result = products.map((product, index) => {
                if(product.phonecompany === "Samsung") return <Product
                    key={index}
                    product={product}
                    onAddToCart = {onAddToCart}
                    onChangeMessage = {onChangeMessage }
                />
            });
        }
        return result;
    }
}

SamsungContainer.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            image: PropTypes.string.isRequired,
            desciption: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            inventory: PropTypes.number.isRequired,
            rating: PropTypes.number.isRequired
        })
    ).isRequired,
    onChangeMessage: PropTypes.func.isRequired
}

const mapStateToProps = state => {
    return {
        products: state.callapi.callapi
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return {
        onAddToCart : (product) =>{
            dispatch(actAddToCart(product, 1));
        },
        onChangeMessage : (message) =>{
            dispatch(actChangeMessage(message));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SamsungContainer);
